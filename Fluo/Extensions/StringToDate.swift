//
//  StringToDate.swift
//  Fluo
//
//  Created by Leonardo Lage on 27/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

extension String {
    func toDate(withFormat format: String = "dd/MM/yyyy HH:mm:ss") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        guard let date = dateFormatter.date(from: self) else {
            preconditionFailure("Take a look to your format")
        }
        return date
    }
    
    func format(with format: String = "HH:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self.toDate())
    }
}
