//
//  URLComponents.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

extension URLComponents {
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
