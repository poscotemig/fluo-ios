//
//  TaskDAO.swift
//  Fluo
//
//  Created by Leonardo Lage on 24/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation
import SQLite

public class TaskDAO {
    private let database: DatabaseManager
    
    init(_ database: DatabaseManager) {
        self.database = database
    }
    
    func saveTasks(tasks: [TaskResponse]) {
        let stmt = try! database.db.prepare("INSERT OR REPLACE INTO tasks (id, name, idProject, idAccountTo, description, tags, createdAt) VALUES (?, ?, ?, ?, ?, ?, ?)")
        for task in tasks {
            try! stmt.run(task.id, task.name, task.idProject, task.idAccountTo, task.description, task.tags, task.createdAt)
        }
    }
    
    func getTasks() -> [TaskResponse] {
        var tasks = [TaskResponse]()
        
        for task in try! database.db.prepare(database.getTasks()) {
            let id = task[TaskTable.id]
            let idProject = task[TaskTable.idProject]
            let idAccountTo = task[TaskTable.idAccountTo]
            let name = task[TaskTable.name]
            let description = task[TaskTable.description]
            let tags = task[TaskTable.tags]
            let createdAt = task[TaskTable.createdAt]
            
            let item = TaskResponse(id: id, name: name, description: description, tags: tags, idProject: idProject, idAccountTo: idAccountTo, createdAt: createdAt)
            tasks.append(item)
        }
        
        return tasks
    }
}
