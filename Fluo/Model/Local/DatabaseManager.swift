//
//  DatabaseManager.swift
//  Fluo
//
//  Created by Leonardo Lage on 24/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation
import SQLite

public class DatabaseManager {
    public var db: Connection
    
    static let TABLE_TASKS = "tasks"
    
    init() {
        let path = NSSearchPathForDirectoriesInDomains(.applicationDirectory, .userDomainMask, true).first! + "/" + Bundle.main.bundleIdentifier!
        
        try! FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        
        db = try! Connection("\(path)/db.sqlite3")
        createTable()
    }
    
    private func createTable() {
        let tasks = getTasks()
        
        try! db.run(tasks.create(ifNotExists: true) { t in
            t.column(TaskTable.id, primaryKey: true)
            t.column(TaskTable.idProject)
//            t.column(TaskTable.idStatus)
//            t.column(TaskTable.idAccountFrom)
            t.column(TaskTable.idAccountTo)
            t.column(TaskTable.name)
            t.column(TaskTable.description)
            t.column(TaskTable.tags)
//            t.column(TaskTable.estimate)
            t.column(TaskTable.createdAt)
//            t.column(TaskTable.startedAt)
//            t.column(TaskTable.deliveredAt)
//            t.column(TaskTable.priority)
        })
    }
    
    public func getTasks() -> Table {
        return Table(DatabaseManager.TABLE_TASKS)
    }
}
