//
//  TaskTable.swift
//  Fluo
//
//  Created by Leonardo Lage on 24/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation
import SQLite

struct TaskTable {
    static let id = Expression<String>("id")
    static let idProject = Expression<String>("idProject")
//    static let idStatus = Expression<Int>("idStatus")
//    static let idAccountFrom = Expression<String>("idAccountFrom")
    static let idAccountTo = Expression<String>("idAccountTo")
    static let name = Expression<String>("name")
    static let description = Expression<String>("description")
    static let tags = Expression<String>("tags")
//    static let estimate = Expression<String>("estimate")
    static let createdAt = Expression<String>("createdAt")
//    static let startedAt = Expression<Int64?>("startedAt")
//    static let deliveredAt = Expression<Int64?>("deliveredAt")
//    static let priority = Expression<Int64>("priority")
}
