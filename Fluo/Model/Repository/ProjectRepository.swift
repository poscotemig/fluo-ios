//
//  ProjectRepository.swift
//  Fluo
//
//  Created by Leonardo Lage on 21/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

class ProjectRepository {
    private let projectApi: ProjectApi
    
    init(projectApi: ProjectApi) {
        self.projectApi = projectApi
    }
    
    func getProjects(token: String, completion: @escaping([ProjectResponse]) -> Void) {
        projectApi.getProjects(token: token) { result in
            switch result {
            case .success(let projects):
                completion(projects)
            case .failure:
                completion([])
            }
        }
    }
    
    static func factory() -> ProjectRepository {
        let network = NetworkManager()
        let api = ProjectApi(network: network)
        return ProjectRepository(projectApi: api)
    }
}
