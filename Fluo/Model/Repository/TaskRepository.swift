//
//  TaskRepository.swift
//  Fluo
//
//  Created by Leonardo Lage on 13/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

class TaskRepository {
    private let taskApi: TaskApi
    private let taskDAO: TaskDAO
    
    private init(taskApi: TaskApi, taskDAO: TaskDAO) {
        self.taskApi = taskApi
        self.taskDAO = taskDAO
    }
    
    func getTasksFromProject(idProject: String, token: String, completion: @escaping ([TaskResponse]) -> Void) {
        taskApi.getTasksFromProject(idProject: idProject, token: token) { (result) in
            switch result {
            case .success(let tasks):
                completion(tasks)
            case .failure:
                completion([])
            }
        }
    }
    
    func getUserTasks(token: String, completion: @escaping ([TaskResponse]) -> Void) {
        if Connectivity.isConnectedToInternet {
            taskApi.getUserTasks(token: token) { (result) in
                switch result {
                case .success(let tasks):
                    self.taskDAO.saveTasks(tasks: tasks)
                    completion(tasks)
                case .failure:
                    completion([])
                }
            }
        } else {
            let tasks = taskDAO.getTasks()
            completion(tasks)
        }
    }
    
    func newTask(token: String, name: String, idProject: String, idAccountTo: String, tags: String, completion: @escaping (Bool) -> Void) {
        taskApi.newTask(token: token, name: name, idProject: idProject, idAccountTo: idAccountTo, tags: tags) { (result) in
            switch result {
            case .success:
                completion(true)
            case .failure(let error):
                completion(false)
                print(error.localizedDescription)
            }
        }
    }
    
    static func factory() -> TaskRepository {
        let api = TaskApi(network: NetworkManager())
        let dao = TaskDAO(DatabaseManager())
        return TaskRepository(taskApi: api, taskDAO: dao)
    }
}
