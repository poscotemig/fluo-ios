//
//  UserRepository.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

class UserRepository {
    
    private let userApi: UserApi
    
    private init(userApi: UserApi) {
        self.userApi = userApi
    }
    
    func forgot(email: String, completion: @escaping (Bool) -> Void) {
        userApi.forgotPassword(email: email) { (result) in
            switch result {
            case .success:
                completion(true)
            case .failure(let error):
                completion(false)
                print(error.localizedDescription)
            }
        }
    }
    
    func login(email: String, password: String, completion: @escaping (Bool) -> Void) {
        userApi.login(email: email, password: password) { (result) in
            switch result {
            case .success(let user):
                self.saveToken(token: user.token)
                self.saveUserId(id: user.id)
                self.saveUserEmail(email: email)
                self.saveUserPassword(password: password)
                completion(true)
                print(user)
            case .failure(let error):
                completion(false)
                print(error.localizedDescription)
            }
        }
    }
    
    func me() {
        if let token = getToken() {
            userApi.me(token: token) { (result) in
                switch result {
                case .success(let user):
                    print(user)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func singUp(name: String, email: String, password: String, completion: @escaping (Bool) -> Void) {
        userApi.signup(name: name, email: email, password: password) { result in
            switch result {
            case .success(let user):
                completion(true)
                print(user)
            case .failure(let error):
                completion(false)
                print(error.localizedDescription)
            }
        }
    }
    
    private func saveToken(token: String) {
        UserDefaults.standard.set(token, forKey: "token")
    }
    
    func getToken() -> String? {
        return UserDefaults.standard.string(forKey: "token")
    }
    
    private func saveUserId(id: String) {
        UserDefaults.standard.set(id, forKey: "userId")
    }
    
    func getUserId() -> String {
        return UserDefaults.standard.string(forKey: "userId")!
    }
    
    private func saveUserEmail(email: String) {
        UserDefaults.standard.set(email, forKey: "userEmail")
    }
    
    func getUserEmail() -> String? {
        return UserDefaults.standard.string(forKey: "userEmail")
    }
    
    private func saveUserPassword(password: String) {
        UserDefaults.standard.set(password, forKey: "userPassword")
    }
    
    func getUserPassword() -> String? {
        return UserDefaults.standard.string(forKey: "userPassword")!
    }
    
    static func factory() -> UserRepository {
        let network = NetworkManager()
        let api = UserApi(network: network)
        return UserRepository(userApi: api)
    }
}
