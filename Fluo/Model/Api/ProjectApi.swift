//
//  ProjectApi.swift
//  Fluo
//
//  Created by Leonardo Lage on 21/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation
import Alamofire

class ProjectApi {
    
    private let network: NetworkManager
    
    init(network: NetworkManager) {
        self.network = network
    }
    
    func getProjects(token: String, completion: @escaping(Result<[ProjectResponse], AFError>) -> Void) {
        network.get(path: "/v1/project", headers: ["token" : token], completion: completion)
    }
}
