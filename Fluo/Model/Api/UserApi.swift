//
//  UserApi.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation
import Alamofire

class UserApi {
    
    private let network: NetworkManager
    
    init(network: NetworkManager) {
        self.network = network
    }
    
    func login(email: String, password: String, completion: @escaping (Result<LoginResponse, AFError>) -> Void ) {
        let parameters = ["email" : email, "password": password]
        network.post(path: "/v1/account/auth", parameters: parameters, completion: completion)
    }
    
    func me(token: String, completion: @escaping (Result<LoginResponse, AFError>) -> Void ) {
        network.get(path: "/v1/account/me", headers: ["token": token], completion: completion)
    }
    
    func signup(name: String, email: String, password: String, completion: @escaping (Result<SignUpResponse, AFError>) -> Void) {
        let parameters = ["name" : name, "email" : email, "password": password]
        network.post(path: "/v1/account", parameters: parameters, completion: completion)
    }
    
    func forgotPassword(email: String, completion: @escaping (Result<Data?, AFError>) -> Void) {
        let parameters = ["email": email]
        network.postEmptyBody(path: "/v1/account/forgot", parameters: parameters, completion: completion)
    }
}
