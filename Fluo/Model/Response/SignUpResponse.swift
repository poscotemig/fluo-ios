//
//  SignUpResponse.swift
//  Fluo
//
//  Created by Leonardo Lage on 09/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

struct SignUpResponse: Codable {
    let id: String
}
