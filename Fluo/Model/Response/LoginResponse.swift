//
//  LoginResponse.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    let id: String
    let name: String
    let email: String
    let token: String
    let active: Bool
}
