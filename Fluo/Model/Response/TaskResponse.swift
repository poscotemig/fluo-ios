//
//  Task.swift
//  Fluo
//
//  Created by Leonardo Lage on 13/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

struct TaskResponse: Codable {
    let id, name, description, tags, idProject, idAccountTo, createdAt: String
}
