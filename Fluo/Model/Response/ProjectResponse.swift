//
//  ProjectResponse.swift
//  Fluo
//
//  Created by Leonardo Lage on 21/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import Foundation

struct ProjectResponse: Codable {
    let id, name: String
    let tasks: Int
}
