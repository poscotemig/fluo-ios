//
//  TodoTableViewCell.swift
//  Fluo
//
//  Created by Leonardo Lage on 06/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell {

    @IBOutlet weak var vStatus: UIView!
    @IBOutlet weak var ivDone: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var ivAlert: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureList(task: TaskResponse) {
        
        lblDescription.text = task.name
        
        lblTime.text = task.createdAt.format()
    }

}
