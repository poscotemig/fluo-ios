//
//  NewTaskViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 20/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class NewTaskViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var parentController: UIViewController? = nil
    private let userRepository = UserRepository.factory()
    private let taskRepository = TaskRepository.factory()
    private let projectRepository = ProjectRepository.factory()
    private var projectSelected: ProjectResponse?
    var projects: [ProjectResponse] = []
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tfName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfDate: UITextField!
    private var rowProjectSelected: Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.isOpaque = true        
        getProjects()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        tfDate.text = dateFormatter.string(from: Date())
    }
    
    @IBAction func closeNewTask(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func beginChooseDate(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .dateAndTime
        datePickerView.minimumDate = Date()
        datePickerView.locale = Locale.init(identifier: "pt_BR")
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(NewTaskViewController.datePickerValueChanged), for: .valueChanged)
    }
    
        
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        tfDate.text = dateFormatter.string(from: sender.date)
    }
    
    func getProjects() {
        projectRepository.getProjects(token: userRepository.getToken()!) { result in
            if result.count > 0 {
                self.projects = result
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func newTask(_ sender: Any) {
        if let name = tfName?.text {
            if name != "" {
                if let project = self.projectSelected {
                    taskRepository.newTask(token: userRepository.getToken()!, name: name, idProject: project.id, idAccountTo: userRepository.getUserId(), tags: "") { result in
                        if result {
                            if let vc = self.parentController as? TodoViewController {
                                vc.getTasks()
                            }
                            else if let vc = self.parentController as? ProjectsViewController {
                                vc.getProjects()
                            }
                            
                            let alert = UIAlertController(title: "Sucesso", message: "Tarefa cadastrada com sucesso", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            })
                            self.present(alert, animated: true)
                        } else {
                            let alert = UIAlertController(title: "Erro", message: "Erro ao cadastrar nova tarefa", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                        }
                    }
                } else {
                    let alert = UIAlertController(title: "Erro", message: "Selecione um projeto", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            } else {
                let alert = UIAlertController(title: "Erro", message: "Informe o nome da tarefa", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addTaskProject_cell", for: indexPath) as! AddTaskProjectCollectionViewCell
        
        cell.configureList(project: projects[indexPath.row])
        
        if indexPath.row == rowProjectSelected {
            cell.backgroundColor = .blue
            cell.lblName.textColor = .white
        } else {
            cell.backgroundColor = .clear
            cell.lblName.textColor = .black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.projectSelected = projects[indexPath.row]
        self.rowProjectSelected = indexPath.row
        collectionView.reloadData()
    }
}
