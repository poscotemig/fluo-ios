//
//  ProjectsCollectionViewCell.swift
//  Fluo
//
//  Created by Leonardo Lage on 10/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class AddTaskProjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureList(project: ProjectResponse) {
        lblName.text = project.name
    }
}
