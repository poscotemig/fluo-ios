//
//  LoginViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    private let userRepository = UserRepository.factory()
    
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfEmail.text = "leonardo.lageso@gmail.com"
        tfPassword.text = "6t84fn09"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func recuperarSenha(_ sender: UIButton) {
        let textEmail = tfEmail?.text
        
        if let email = textEmail {
            if email != "" {
                userRepository.forgot(email: email) { result in
                    if result {
                        let alert = UIAlertController(title: "Sucesso", message: "Foi enviado para o seu email uma nova senha.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    } else {
                        let alert = UIAlertController(title: "Erro", message: "Erro ao enviar senha para o seu email.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Erro", message: "Informe o email", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        let textEmail = tfEmail?.text
        let textPassword = tfPassword?.text
        
        if let email = textEmail, let password = textPassword {
            if email != "" && password != "" {
                userRepository.login(email: email, password: password) { result in
                    if result {
                        self.performSegue(withIdentifier: "mainSegue", sender: nil)
                    } else {
                        let alert = UIAlertController(title: "Erro", message: "Email ou senha incorretos", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Erro", message: "Informe o email e a senha", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))                
                self.present(alert, animated: true)
            }
        }
    }
}
