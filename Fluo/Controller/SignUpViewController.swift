//
//  SignUpViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 02/07/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    private let userRepository = UserRepository.factory()
    
    @IBOutlet weak var tfName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        let textName = tfName? .text
        let textEmail = tfEmail?.text
        let textPassword = tfPassword?.text
        
        if let name = textName, let email = textEmail, let password = textPassword {
            if name != "" && email != "" && password != "" {
                userRepository.singUp(name: name, email: email, password: password) { result in
                    if result {
                        let alert = UIAlertController(title: "Sucesso", message: "Cadastro realizado com sucesso", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        self.present(alert, animated: true)
                        
                    } else {
                        let alert = UIAlertController(title: "Erro", message: "Erro na hora de cadastrar", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Erro", message: "Informe o nome, email e a senha", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        }
        
    }
}
