//
//  ProjectsViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 10/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    private let projectRespository = ProjectRepository.factory()
    private let usuarioRepository = UserRepository.factory()
    private var projects: [ProjectResponse] = []
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getProjects()
    }
    
    func getProjects() {
        projectRespository.getProjects(token: usuarioRepository.getToken()!) { result in
            if result.count > 0 {
                self.projects = result
                self.collectionView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "project_cell", for: indexPath) as! ProjectCollectionViewCell
        
        cell.configureList(project: projects[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let project = projects[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProjectTasksViewController") as! ProjectTasksViewController
        viewController.idProjectSelected = project.id
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}
