//
//  ProjectTasksViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 17/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class ProjectTasksViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private let userRepository = UserRepository.factory()
    private let taskRepository = TaskRepository.factory()
    let cellReuseIdentifier = "taskTableViewCell"
    var idProjectSelected: String? = nil
    var tasks: [TaskResponse] = []
    
    @IBOutlet weak var tasksTableView: UITableView!
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTasksFromProject()
    }
    
    func getTasksFromProject() {
        taskRepository.getTasksFromProject(idProject: idProjectSelected!, token: userRepository.getToken()!) { result in
            if result.count > 0 {
                self.tasks = result
                self.tasksTableView.reloadData()
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tasks.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 14
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier, for: indexPath) as! TodoTableViewCell
        
        // add border and color
        cell.backgroundColor = UIColor.white
        cell.layer.borderWidth = 0
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        
        cell.configureList(task: tasks[indexPath.section])
        
        return cell
    }

}
