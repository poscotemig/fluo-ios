//
//  ProjectCollectionViewCell.swift
//  Fluo
//
//  Created by Leonardo Lage on 10/09/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class ProjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblNumberTasks: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureList(project: ProjectResponse) {
        lblProjectName.text = project.name
        lblNumberTasks.text = "\(project.tasks) tasks"
    }
    
}
