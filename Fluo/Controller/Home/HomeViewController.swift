//
//  HomeViewController.swift
//  Fluo
//
//  Created by Leonardo Lage on 06/08/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITabBarDelegate {

    private var selectedViewController: UIViewController? = nil
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var lblHello: UILabel!
    @IBOutlet weak var lblTasksToday: UILabel!
    private lazy var todoViewController: TodoViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TodoViewController") as! TodoViewController
        
        return viewController
    }()
    private lazy var projectsViewController: ProjectsViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ProjectsViewController") as! ProjectsViewController
        
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar?.selectedItem = tabBar.items![0] as UITabBarItem
        updateView()
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        updateView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.selectedViewController = segue.destination
    }
    
    @IBAction func openNewTask(_ sender: Any) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "newTask") as? NewTaskViewController {
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.parentController = self.selectedViewController
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {
        if tabBar.selectedItem!.title == "Projects" {
            self.selectedViewController = projectsViewController
            self.remove(asChildViewController: todoViewController)
            self.add(asChildViewController: projectsViewController)
        } else {
            self.selectedViewController = todoViewController
            self.remove(asChildViewController: projectsViewController)
            self.add(asChildViewController: todoViewController)
        }
    }
}
