//
//  ViewController.swift
//  exercicio1
//
//  Created by Leonardo Lage on 14/05/19.
//  Copyright © 2019 Onegreen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let userRepository = UserRepository.factory()
//    private let userRepository = UserRepository.factory()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        login()
    }
    
    func login() {
        if let email = userRepository.getUserEmail(), let password = userRepository.getUserPassword() {
            userRepository.login(email: email, password: password) { result in
                if result {
                    if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") {
                        self.present(viewController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
